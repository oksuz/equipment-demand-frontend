import React, { useMemo } from 'react';
import useEquipmentDemands from './hooks/useEquipmentDemands';
import Layout, { Content, Footer, Header } from 'antd/lib/layout/layout';
import { Table } from 'antd';
import { dateRenderer } from './utils/tableRenderer';
import EquipmentStatus from './components/EquipmentStatus';
import 'antd/dist/antd.css';
import './style/app.css';

export default function App() {
  const { loading, demands } = useEquipmentDemands();

  const columns = useMemo(() => {
    
    function getCountFromReturningRental(equipmentId, rentalIds = []) {
      return demands.data
        .filter((d) => {
          return rentalIds.indexOf(d.bookingId) > -1 && d.equipmentId === equipmentId;
        })
        .reduce((acc, d) => {
          acc += d.desiredQuantity;
          return acc;
        }, 0);
    }

    return [
      {
        dataIndex: 'bookingId',
        key: 'bookingId',
        title: '#'
      },
      {
        key: 'status',
        title: 'Status',
        render(row) {
          return <EquipmentStatus record={row} getReturningRentalCount={getCountFromReturningRental} />;
        }
      },
      {
        dataIndex: 'equipmentName',
        key: 'equipmentName',
        title: 'Equipment'
      },
      {
        dataIndex: 'desiredQuantity',
        key: 'desiredQuantity',
        title: 'Desired Quantity'
      },
      {
        dataIndex: 'bookFrom',
        key: 'bookFrom',
        title: 'Book From'
      },
      {
        dataIndex: 'returnTo',
        key: 'returnTo',
        title: 'Return To'
      },
      {
        dataIndex: 'bookDate',
        key: 'bookDate',
        title: 'Rental Start',
        render: dateRenderer
      },
      {
        dataIndex: 'returnDate',
        key: 'returnDate',
        title: 'Rental End',
        render: dateRenderer
      }
    ].map((c) => {
      c.align = 'center';
      return c;
    });
  }, [demands.data]);

  return (
    <Layout>
      <Header style={{ color: 'white' }}>Equipment Demand Dashboard</Header>
      <Content style={{ backgroundColor: 'white' }}>
        <Table
          bordered
          loading={loading}
          dataSource={demands.data}
          columns={columns}
          rowKey={(row) => {
            return row.bookingId;
          }}
          pagination={{
            position: ['bottomCenter', 'bottomCenter'],
            defaultPageSize: 10,
            defaultCurrent: 1,
            pageSizeOptions: [10, 20, 50, 100, 250, 500],
            hideOnSinglePage: false,
            showSizeChanger: true
          }}
        ></Table>
      </Content>
      <Footer>&copy; 2021 RoadSurfer</Footer>
    </Layout>
  );
}
