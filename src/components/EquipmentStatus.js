import React from 'react';
import { Badge, Tooltip } from 'antd';

function EquipmentStatus({ record, getReturningRentalCount }) {
  const { requiredQuantity, equipmentId, equipmentWillReturnFromRental } = record;
  const returningRentalItemCount = getReturningRentalCount(equipmentId, equipmentWillReturnFromRental);

  let badge, text;
  if (requiredQuantity === 0) {
    badge = 'success';
  } else if (requiredQuantity > 0 && requiredQuantity <= returningRentalItemCount) {
    badge = 'warning';
    text = `${requiredQuantity} more item(s) required (It will be available when ${equipmentWillReturnFromRental} return)`;
  } else {
    badge = 'error';
    text = `${requiredQuantity} more item(s) required`;
  }

  return (
    <Tooltip title={text} placement="rightTop">
      <Badge status={badge} />
    </Tooltip>
  );
}

export default React.memo(EquipmentStatus);
