import { useState, useEffect } from 'react';
import http from '../provider/http';

export default function useEquipmentDemands() {
  const [demands, setDemands] = useState({ data: [], error: null });
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    fetch();

    async function fetch() {
      try {
        setLoading(true);
        const { data } = await http.get('/booking/equipment/demands');
        setDemands({ ...demands, data });
      } catch (e) {
        setDemands({ ...demands, error: e.message });
      } finally {
        setLoading(false);
      }
    }
  }, []);

  return { loading, demands };
}
