import moment from 'moment';


export function dateRenderer(val) {
  return moment(val).format('DD.MM.YYYY');  
}
