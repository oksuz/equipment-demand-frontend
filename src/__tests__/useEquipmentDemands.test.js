import 'regenerator-runtime/runtime'
import { render, waitFor } from "@testing-library/react";
import React from "react";
import useEquipmentDemands from "../hooks/useEquipmentDemands";
import http from "../provider/http";

const ExampleComponent = () => {
  const { loading, demands } = useEquipmentDemands();
  return null;
};

jest.mock('../provider/http')

describe('useEquipmentDemands', () => {

  beforeEach(() => {
    React.useEffect.mockClear && React.useEffect.mockClear();
  });

  it('initial state', () => {
    const useStateMock = jest.fn();
  
    const spyUseEffect = jest.spyOn(React, 'useEffect').mockImplementationOnce(() => {
    });
  
    const spyeUseState = jest.spyOn(React, 'useState').mockImplementation((val) => {
      return [val, useStateMock];
    });
    
    render(<ExampleComponent />);
  
    expect(spyUseEffect).toHaveBeenCalled();
    expect(spyeUseState).toHaveBeenNthCalledWith(1, { data: [], error: null });
    expect(spyeUseState).toHaveBeenNthCalledWith(2, false);
  });

  it('invoke correct order', async () => {
    const setDemands = jest.fn();
    const setLoading = jest.fn();

    React.useState = jest.fn()
    .mockImplementationOnce((val) => {
      return [val, setDemands];
    })
    .mockImplementationOnce((val) => {
      return [val, setLoading];
    });


    http.get.mockImplementationOnce(() => {
      return Promise.resolve({ data: [] });
    });

    render(<ExampleComponent />);

    await waitFor(() => {
      expect(setLoading).toHaveBeenCalledWith(true);
      expect(http.get).toHaveBeenCalledWith('/booking/equipment/demands');
      expect(setDemands).toHaveBeenCalledWith({ data: [], error: null });
    });
  });

});