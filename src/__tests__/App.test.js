import 'regenerator-runtime/runtime'
import React from 'react';
import App from '../App';
import { render, fireEvent, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom'
import useEquipmentDemands from '../hooks/useEquipmentDemands';
import EquipmentStatus from '../components/EquipmentStatus';
import * as util from '../utils/tableRenderer' 
import { mockResponse } from '../__mocks__/response';

Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: jest.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(), // deprecated
    removeListener: jest.fn(), // deprecated
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn()
  }))
});


jest.mock('../hooks/useEquipmentDemands');
jest.mock('../components/EquipmentStatus', () => () => {
  return <div></div>
});


const setup = () => {
  const utils = render(<App />);
};

describe('<App />', () => {

  beforeEach(() => {
    useEquipmentDemands.mockClear();
  });


  it('checks header', () => {
    useEquipmentDemands.mockImplementation(() => {
      return {
        loading: false,
        demands: { data: [], error: null }
      }
    });


    const { queryByText } = render(<App />);
    expect(queryByText('Equipment Demand Dashboard')).toBeInTheDocument();
    expect(queryByText(/2021 RoadSurfer/)).toBeInTheDocument();
  })


  it('display table', () => {
    useEquipmentDemands.mockImplementation(() => {
      return {
        loading: false,
        demands: { data: mockResponse, error: null }
      }
    });

    const mockDateRender = jest.spyOn(util, 'dateRenderer');
    const { queryByText } = render(<App />);
    
    expect(mockDateRender).toHaveBeenCalled();

    // table header

    expect(queryByText('Status')).toBeInTheDocument();
    expect(queryByText('Equipment')).toBeInTheDocument();
    expect(queryByText('Desired Quantity')).toBeInTheDocument();
    expect(queryByText('Book From')).toBeInTheDocument();
    expect(queryByText('Return To')).toBeInTheDocument();
    expect(queryByText('Rental Start')).toBeInTheDocument();
    expect(queryByText('Rental End')).toBeInTheDocument();

  });
});